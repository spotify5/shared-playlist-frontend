import { PlaylistService } from './../../services/playlist.service';
import { FakeTrackService } from './../../services/fake-track.service';
import { Track } from '../../types/track';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-party-marty',
  templateUrl: './party-marty.component.html',
  styleUrls: ['./party-marty.component.css'],
})
export class PartyMartyComponent implements OnInit {
  public tracks: Track[] = [];

  public selectedTracks: Track[] = [];

  //song1 = {name: 'Yellow Submarine', artists: [name: 'The Beatles'], duration: 224, popularity: 100} as Song

  constructor(
    private trackService: FakeTrackService,
    private playlistService: PlaylistService
  ) {}

  ngOnInit(): void {
    this.trackService.getAllTracks().subscribe((res) => {
      res.forEach((track) => this.tracks.push(track));
    });
  }

  onChange(track: Track, checked: boolean): void {
    if (checked) {
      this.selectedTracks.push(track);
    } else {
      this.selectedTracks = this.selectedTracks.filter(
        (selected) => track !== selected
      );
    }
  }

  onClickProposeDeletion() {
    //this.playlistService.proposeSelectedSongsForDeletion(this.selectedTracks);
  }
}
