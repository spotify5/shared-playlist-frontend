import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartyMartyComponent } from './party-marty.component';

describe('PartyMartyComponent', () => {
  let component: PartyMartyComponent;
  let fixture: ComponentFixture<PartyMartyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartyMartyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartyMartyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
