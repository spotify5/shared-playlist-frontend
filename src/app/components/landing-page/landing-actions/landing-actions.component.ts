import { LandingAction } from './landing-action/landing-action.d';
import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-landing-actions',
  templateUrl: './landing-actions.component.html',
  styleUrls: ['./landing-actions.component.css'],
})
export class LandingActionsComponent implements OnInit {
  landingActions: LandingAction[] = [
    { name: 'Party Marty', routerLink: '/party-marty' },
    { name: 'zweiter Eintrag', routerLink: '/link2' },
    { name: 'dritter Eintrag', routerLink: '/link3' },
  ];

  constructor() {}

  ngOnInit(): void {}
}
