import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-landing-action',
  templateUrl: './landing-action.component.html',
  styleUrls: ['./landing-action.component.css'],
})
export class LandingActionComponent implements OnInit {
  @Input()
  name: string;

  @Input()
  routerLink: string;

  constructor() {}

  ngOnInit(): void {}
}
