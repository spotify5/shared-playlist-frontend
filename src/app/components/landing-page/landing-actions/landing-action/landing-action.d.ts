export interface LandingAction {
  name: string;
  routerLink: string;
}
