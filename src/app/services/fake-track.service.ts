import { TrackService } from './track.service';
import { Injectable } from '@angular/core';
import { Track } from '../types/track';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FakeTrackService implements TrackService {
  private playlist: Track[] = [];

  constructor() {
    this.playlist.push({
      id: '4',
      name: 'Yellow Submarine',
      artists: [
        { id: '122', name: 'The Beatles' },
        { id: '121', name: 'and friends' },
      ],
      duration: 224,
      popularity: 78,
    });
    this.playlist.push({
      id: '1',
      name: 'Green Submarine',
      artists: [{ id: '123', name: 'The Fake Beatles' }],
      duration: 3344,
      popularity: 12,
    });
    this.playlist.push({
      id: '2',
      name: 'Egal',
      artists: [{ id: '124', name: 'Michael Wendler' }],
      duration: 1234,
      popularity: 44,
    });
    this.playlist.push({
      id: '3',
      name: 'Der letzte Sirtaki',
      artists: [{ id: '125', name: 'Rex Gildo' }],
      duration: 267,
      popularity: 99,
    });
  }

  getTracksByName(name: string): Observable<Track[]> {
    return of(this.playlist.filter((x) => !x.name.startsWith(name)));
  }

  getAllTracks(): Observable<Track[]> {
    return of(this.playlist);
  }
}
