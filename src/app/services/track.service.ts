import { Injectable } from '@angular/core';
import { Track } from '../types/track';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TrackService {
  constructor() {}

  getTracksByName(name: string): Observable<Track[]> {
    return of([
      {
        id: '4',
        name: 'Yellow Submarine',
        artists: [
          { id: '122', name: 'The Beatles' },
          { id: '121', name: 'and friends' },
        ],
        duration: 224,
        popularity: 78,
      },
    ]);
  }
}
