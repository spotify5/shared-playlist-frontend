import { Track } from './../types/track.d';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class PlaylistService {
  API_BASE_URL = 'localhost:3000';
  PLAYLIST_ID = '12345';

  constructor(private http: HttpClient) {}

  getPlaylistById(playlistId: string): Track[] {
    const headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');

    this.http.get(`${this.API_BASE_URL}/playlist/${playlistId}`).subscribe();

    let tracks: Track[] = [];
    return tracks;
  }

  proposeSelectedSongsForDeletion(
    tracksToDelete: Track[],
    playlistId: string
  ): void {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Access-Control-Allow-Origin', '*');

    this.http.post(
      `${this.API_BASE_URL}/deleteProposals`,
      { tracksToDelete, playlistId },
      { headers }
    );
  }
}
