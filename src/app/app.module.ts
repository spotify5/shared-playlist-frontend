import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { LandingActionsComponent } from './components/landing-page/landing-actions/landing-actions.component';
import { LandingActionComponent } from './components/landing-page/landing-actions/landing-action/landing-action.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartyMartyComponent } from './components/party-marty/party-marty.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    LandingActionsComponent,
    LandingActionComponent,
    PartyMartyComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, NgbModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
