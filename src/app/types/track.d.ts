import { NameObject } from './generics';

export interface Track extends NameObject {
  artists: Artist[];
  duration: number;
  popularity: number;
}

export interface Artist extends NameObject {}
