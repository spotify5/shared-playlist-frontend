export interface NameObject {
  id: string;
  name: string;
}
